import Vue from 'vue'
import VueRouter from 'vue-router'
import LatencyTest from '../views/LatencyTest.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'LatencyTest',
    component: LatencyTest
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

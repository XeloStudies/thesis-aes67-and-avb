import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    control: {
      loading: false,
      isInit: false,
      isTesting: false,
    },
    test: {
      sender: null,
      receivers: [],
      config: {
        channels: 1,
        sampleRate: 44800,
        function: "Pulse",
        framesPerPacket:48,
        runs:10,
        samples:1000000,
      },
    },
    hosts: [],
    results: [],
  },
  getters: {
    senderObject(state) {
      if(state.test.sender != null){
        const host = state.hosts.find(x => x.id == state.test.sender.hostId)
        return {
          name: host.name,
          device: host.devices.find( x=> x.id == state.test.sender.id).name
        }
      }
      return null
    },
    mappedReceivers(state){
      if(state.test.receivers.length > 0 ){
        return state.test.receivers.map(r => {
         return {
          device: r.name,
          name: state.hosts.find(x => x.id == r.hostId).name
         }
        });
      }
      return []
    },
    hostSelection(state){
      return state.test.sender != null && state.test.receivers.length > 0 
    }
  },
  mutations: {
    setHosts(state, h){
      state.hosts = h
    },
    setResults(state, r){
      state.control.isTesting = false //(r.len == state.test.config.runs-1)
      state.control.loading = false //(r.len == state.test.config.runs-1)
      state.results = r
    },
    setSender(state, s){
      if (s.sender == true  && state.test.sender == null ) {
        state.test.sender = {id : s.id, hostId: s.hostId}
      } else {
        state.test.sender = null  
      }
    },
    setReceiver(state, l){
      if (state.test.receivers.find( x => x.id == l.id) != null  && l.receiver == false) { 
        state.test.receivers = state.test.receivers.filter( x => x.id != l.id)
      }
      if(l.receiver == true) {
        state.test.receivers.push(l)
      }
    },
    setChannels(state, c){
      console.log(c)
      state.test.config.channels=c
    },
    setFunction(state, f){
      state.test.config.function=f
    },
    setSampleRate(state, s){
      state.test.config.sampleRate=s
    },
    setRuns(state, r){
      state.test.config.runs=r
    },
    setSamples(state, s){
      state.test.config.samples=s
    },
    setFramesPerPacket(state, f){
      state.test.framesPerPacket=f
    },
    setConfig(state, conf) {
      state.test.conf = conf
    },
    resetTest(state){
      state.test.receivers = []
      state.test.sender = null
      state.test.config.channels = 1
      state.test.config.function = "Pulse"
      state.test.config.sampleRate = 44800
      state.test.config.framesPerPacket = 48
      state.test.config.runs = 10
      state.test.config.samples = 1000000
      state.control.isInit = false
      state.control.isTesting = false
      state.control.loading = false 
      state.hosts.forEach( h => h.devices.forEach(
        d => { d.sender = false, d.receiver = false}
      ))
      state.results = []
    },
  },
  actions: {  
    async getHosts(context){
      await axios.get(process.env.BASE_URL + "api/hosts").then(
        e => { 
        console.log(e.data)
        context.commit('setHosts', e.data);
      }
      ).catch( e => console.error(e))
    },
    startTest(context){
      context.state.control.loading=true
      axios.post("api/rta/test").then(() => {
        context.state.control.isTesting=true
      })
    },
    initTest(context){
      context.state.control.loading=true
      axios.post("api/rta/init",context.state.test).then(
        () => {
          context.state.control.loading = false
          context.state.control.isInit = true
      })
    },
    cancel(context){
      axios.post("api/rta/cancel").then(
        context.commit("resetTest")
      )
    },
    getResults(context){
      axios.get("api/rta/results").then(
        (e) => {
          if (e.status == 102) {
            console.debug("got 102")
            return
          }
          if (e.status == 204){
            console.debug("No test initialized")
            return
          }
          console.log(e.data)
          if(e.data.run != null){
            context.commit('setResults', e.data.run);
          }
        }).catch( err => {
          console.error("Doing an error...", err)
        })
    }
  },
  modules: {
  }
})

package rtaudiohandle

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"hash/fnv"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"
)

// HostInfo describes a host with its DeviceInfos
type HostInfo struct {
	Name            string        `json:"name"`
	Id              uint32        `json:"id"`
	OperatingSystem string        `json:"os"`
	IpAddress       string        `json:"ip"`
	Devices         []*DeviceInfo `json:"devices"`
}

func (h *HostInfo) URL() string {
	return "http://" + h.IpAddress + ":9090/stream"
}

// DeviceInfo maps the rtaudio.DeviceInfo Struct to be json compatible.
type DeviceInfo struct {
	ID                  uint   `json:"id"`
	HostId              uint32 `json:"hostId"`
	Name                string `json:"name"`
	Probed              bool   `json:"probed"`
	NumOutputChannels   int    `json:"numOutputChannels"`
	NumInputChannels    int    `json:"numInputChannels"`
	NumDuplexChannels   int    `json:"numDuplexChannels"`
	IsDefaultOutput     bool   `json:"isDefaultOutput"`
	IsDefaultInput      bool   `json:"isDefaultInput"`
	PreferredSampleRate uint   `json:"preferredSampleRate"`
	SampleRates         []int  `json:"sampleRates"`
}

func (h *HostInfo) SetId() {
	fmt.Println("Setting Host ID")

	h.Id = hash(h.Name)
	for i := range h.Devices {
		h.Devices[i].HostId = h.Id
	}
}

func hash(s string) uint32 {
	h := fnv.New32a()
	if _, err := h.Write([]byte(s)); err != nil {
		panic("Could not Hash!")
	}
	return h.Sum32()
}

func (h *HostInfo) GetDevice(id uint) DeviceInfo {
	for _, device := range h.Devices {
		if device.ID == id {
			return *device
		}
	}
	return DeviceInfo{}
}

// Controller is the interface to the outer world.
type Controller struct {
	hosts      map[uint32]*HostInfo
	streamer   uint32
	receivers  []uint32
	http       *http.Client
	runs       int
	testing    sync.Mutex
	resultsWG  sync.WaitGroup
	TestMatrix *TestMatrix
}

func NewController() *Controller {
	myClient := &http.Client{
		Timeout: time.Duration(3) * time.Second,
	}
	hm := make(map[uint32]*HostInfo)
	return &Controller{hosts: hm, http: myClient}
}

func (ct *Controller) Register(c *gin.Context) {
	var host HostInfo
	if err := c.BindJSON(&host); err != nil {
		fmt.Println(err)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
	} else {
		host.SetId()
		ct.hosts[host.Id] = &host
		if len(ct.hosts) == 0 {
			fmt.Println("No Hosts bind ...")
		} else {
		}
		c.JSON(http.StatusCreated, host.Id)
	}
}

func (ct *Controller) Index(c *gin.Context) {
	r := make([]HostInfo, 0, len(ct.hosts))
	for _, h := range ct.hosts {
		r = append(r, *h)
	}
	c.IndentedJSON(http.StatusOK, r)
}

func (ct *Controller) InitTest(c *gin.Context) {
	// Reset hosts
	ct.receivers = []uint32{}
	ct.streamer = 0
	var config TestConfig
	if err := c.BindJSON(&config); err != nil {
		fmt.Println("could not bind config")
	}
	ct.runs = config.Config.Runs

	h := ct.hosts[config.Sender.HostId]
	da, err := NewStreamerWithConfig(uint(config.Sender.Id), config.Config).Marshal()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Setting Host ", h.Name, " as Sender.")
	resp, err := ct.http.Post("http://"+h.IpAddress+":9090/init", "application/json", bytes.NewBuffer(da))
	if err != nil {
		fmt.Println("Got error, ", err)
	}
	if resp.StatusCode == http.StatusOK {
		ct.streamer = h.Id
	} else {
		fmt.Println("StatusCode differs,", resp.StatusCode, " ", resp.Status)
		log.Fatal("Something went wrong.")
	}
	for _, r := range config.Receivers {
		h = ct.hosts[r.HostId]
		da, err := NewReceiverWithConfig(uint(config.Sender.Id), config.Config).Marshal()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Setting Host ", h.Name, " as Receiver.")
		resp, err := ct.http.Post("http://"+h.IpAddress+":9090/init", "application/json", bytes.NewBuffer(da))
		if resp.StatusCode == http.StatusOK {
			ct.receivers = append(ct.receivers, h.Id)
		} else {
			log.Fatal("Something went wrong.")
		}
	}

	// Create TestMatrix
	ct.TestMatrix = NewTestMatrix(config.Config.Runs, int(config.Config.Channels))

	c.Status(http.StatusOK)
}

func (ct *Controller) Cancel(c *gin.Context) {
	if !ct.testing.TryLock() {
		fmt.Println("Unlocking")
		ct.testing.Unlock()
	}
	ct.testing.Unlock()
	ct.streamer = 0
	ct.receivers = nil
	ct.runs = 0
	c.Status(http.StatusOK)
}

func (ct *Controller) RunTest(c *gin.Context) {
	fmt.Println("Try Lock...")
	if ct.testing.TryLock() {
		fmt.Println("Tests ready to go")
		go ct.runTests()
		c.Status(http.StatusOK)
	} else {
		fmt.Println(" ...actually busy")
		c.Status(http.StatusLocked)
	}
}

func (ct *Controller) runTests() {
	fmt.Println("Subroutine Started")
	for i := 0; i < ct.runs; i++ {
		fmt.Println("Doing testrun:", i)
		for _, s := range ct.receivers {
			fmt.Println("Starting Receiver by:", s)
			ct.resultsWG.Add(1)

			checkHttpGet(ct.http.Get(ct.hosts[s].URL() + "/" + strconv.FormatInt(int64(i), 10)))
		}
		fmt.Println("Starting Streamer by:", ct.streamer)
		ct.resultsWG.Add(1)
		checkHttpGet(ct.http.Get(ct.hosts[ct.streamer].URL() + "/" + strconv.FormatInt(int64(i), 10)))
		ct.resultsWG.Wait()

		fmt.Println("Run finished ...")
		go ct.TestMatrix.Tests[i].Calc()
		fmt.Println(ct.TestMatrix.Tests[i].Res)
		time.Sleep(time.Duration(2) * time.Second)
	}
	ct.testing.Unlock()
}

func (ct *Controller) Results(c *gin.Context) {
	var res Result
	if err := c.BindJSON(&res); err != nil {
		fmt.Println("could not bind result \n", err)
	}
	if res.HostId == ct.streamer {
		ct.TestMatrix.SetSender(res)
	} else {
		ct.TestMatrix.SetReceiver(res)
	}

	ct.resultsWG.Done()
	c.Status(http.StatusOK)
}

func (ct *Controller) GetResults(c *gin.Context) {
	if ct.TestMatrix == nil {
		c.Status(http.StatusNoContent)
		return
	}
	if ct.TestMatrix.IsBusy() {
		fmt.Println("Currently Busy ... ")
		c.Status(http.StatusProcessing)
		return
	}
	c.IndentedJSON(http.StatusOK, ct.TestMatrix)
}

type ConfigParams struct {
	Channels        uint   `json:"channels"`
	SampleRate      uint   `json:"sampleRate"`
	Function        string `json:"function"`
	Runs            int    `json:"runs"`
	Samples         uint64 `json:"samples"`
	FramesPerPacket uint   `json:"framesPerPacket"`
}

type StreamParams struct {
	DeviceID        uint   `json:"deviceId"`
	NumChannels     uint   `json:"numChannels"`
	FirstChannel    uint   `json:"firstChannel"`
	Sender          bool   `json:"sender"`
	Receiver        bool   `json:"receiver"`
	FramesPerPacket uint   `json:"framesPerPacket"`
	SampleRate      uint   `json:"sampleRate"`
	Function        string `json:"function"`
	Samples         uint64 `json:"samples"`
	Runs            int    `json:"runs"`
}

func NewStreamerWithConfig(deviceID uint, p ConfigParams) *StreamParams {
	return &StreamParams{
		DeviceID:        deviceID,
		NumChannels:     p.Channels,
		Sender:          true,
		Receiver:        false,
		FramesPerPacket: p.FramesPerPacket,
		SampleRate:      p.SampleRate,
		Function:        p.Function,
		Samples:         p.Samples,
		Runs:            p.Runs,
	}
}

func NewReceiverWithConfig(deviceID uint, p ConfigParams) *StreamParams {
	return &StreamParams{
		DeviceID:        deviceID,
		NumChannels:     p.Channels,
		Sender:          false,
		Receiver:        true,
		FramesPerPacket: p.FramesPerPacket,
		SampleRate:      p.SampleRate,
		Function:        p.Function,
		Samples:         p.Samples,
		Runs:            p.Runs,
	}
}

func (p *StreamParams) Marshal() ([]byte, error) {
	jsd, err := json.Marshal(p)
	if err != nil {
		return nil, err
	}
	return jsd, nil
}

type TestConfig struct {
	Sender struct {
		Id     uint32 `json:"id"`
		HostId uint32 `json:"hostId"`
	} `json:"sender"`
	Receivers []DeviceInfo `json:"receivers"`
	Config    ConfigParams `json:"config"`
}

func checkHttpGet(res *http.Response, err error) {
	if err != nil {
		fmt.Println("Got Error for Request!!!", err)
		return
	}
	if res.StatusCode != http.StatusOK {
		fmt.Println("Status is not OK, it is: ", res.Status)
	}
}

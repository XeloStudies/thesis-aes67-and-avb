package rtaudiohandle

import (
	"fmt"
	"math"
	"reflect"
	"sync"
	"time"
)

type Result struct {
	HostId       uint32          `json:"id"`           // HostId identifies the id of the host
	RunId        uint64          `json:"runId"`        // RunId is the run id of the current test
	Points       [][]*SmallPoint `json:"points"`       // Points are the measured data points
	TimeStart    int64           `json:"timeStart"`    // TimeStart, the run start
	TimeStopped  int64           `json:"timeStopped"`  // TimeStopped, end of the run
	SamplesTotal uint64          `json:"samplesTotal"` // SamplesTotal, total amount of samples per test run
	Duration     int64           `json:"duration"`     // Duration of Stream
	Latency      int             `json:"latency"`      // Latency at the end of the Stream
}

type SmallPoint struct {
	TS        int64       `json:"timestamp"`
	Sample    uint64      `json:"sample"`
	Value     interface{} `json:"value"`
	Latency   int         `json:"latency"` // Latency at the at the current Point
	Underflow bool        `json:"underflow"`
	Overflow  bool        `json:"overflow"`
	Channel   uint64      `json:"channel"`
}

type RunResult struct {
	RunId             int       `json:"runId"`
	HostId            uint32    `json:"hostId"`
	DiffSamples       uint64    `json:"diffSamples"`
	DiffStart         int64     `json:"diffStart"`
	DiffStop          int64     `json:"diffStop"`
	DiffDuration      uint64    `json:"diffDuration"`
	Mean              []float64 `json:"mean"`
	StandardDeviation []float64 `json:"standardDeviation"`
}

type TestRun struct {
	Id        int     `json:"runId"`
	Channels  int     `json:"channels"`
	Sender    *Result `json:"sender"`
	receivers sync.Map
	Receivers map[uint32]Result `json:"receivers"`
	Res       []*RunResult      `json:"result"`
	m         sync.Mutex
}

func NewTestRun(id, channels int) *TestRun {

	tr := TestRun{
		Id:        id,
		Channels:  channels,
		Receivers: make(map[uint32]Result),
	}

	return &tr
}

func (r *TestRun) Calc() {
	f := func(k interface{}, rec any) bool {
		if reflect.TypeOf(rec) == reflect.TypeOf(Result{}) {
			rc := rec.(Result)
			var res RunResult
			res.RunId = r.Id
			res.HostId = rc.HostId
			res.DiffSamples = rc.SamplesTotal - r.Sender.SamplesTotal
			res.DiffStart = r.Sender.TimeStart - rc.TimeStart // Receiver Started before Sender
			res.DiffStop = rc.TimeStopped - r.Sender.TimeStopped
			res.DiffDuration = uint64(rc.Duration - r.Sender.Duration)
			res.Mean = make([]float64, r.Channels)
			res.StandardDeviation = make([]float64, r.Channels)
			// Calc for every Channel Mean and Deviation
			for c := 0; c < r.Channels; c++ {
				mean, sd := MeanDeviation(r.Sender.Points[c], rc.Points[c])
				res.Mean[c] = mean
				res.StandardDeviation[c] = sd
			}
			r.Res = append(r.Res, &res)
		}
		return true
	}
	for {
		if r.m.TryLock() {
			r.receivers.Range(f)
			r.m.Unlock()
			break
		}
		time.Sleep(time.Duration(1) * time.Millisecond)
	}
}

func (r *TestRun) isBusy() bool {
	if r.m.TryLock() {
		r.m.Unlock()
		return false
	}
	return true
}

func MeanDeviation(sender, receiver []*SmallPoint) (float64, float64) {
	diff := make([]int64, len(sender))
	mean := 0.0
	sd := 0.0
	for i, s := range sender {
		diff[i] = receiver[i].TS - s.TS
		mean += float64(diff[i])
	}
	mean = mean / float64(len(diff))

	for _, d := range diff {
		sd += math.Pow(float64(d)-mean, 2)
	}
	sd = math.Sqrt(sd / float64(len(diff)))
	return mean, sd
}

type TestMatrix struct {
	Tests []*TestRun `json:"run"`
}

func NewTestMatrix(runs int, channels int) *TestMatrix {
	tm := TestMatrix{}
	for i := 0; i < runs; i++ {
		tm.Tests = append(tm.Tests, NewTestRun(i, channels))
	}
	return &tm
}

func (tm *TestMatrix) IsBusy() bool {
	for i, test := range tm.Tests {
		if i == 0 {
			if test.Res == nil {
				fmt.Println("First Test Result nil")
				return true
			}
		}
		if test.isBusy() {
			fmt.Println("Test currently in Calculation")
			return true
		}
	}
	fmt.Println("Not busy")
	return false
}

func (tm *TestMatrix) SetSender(res Result) {
	tm.Tests[res.RunId].Sender = &res
}

func (tm *TestMatrix) SetReceiver(res Result) {
	tm.Tests[res.RunId].receivers.Store(res.HostId, res)
	tm.Tests[res.RunId].Receivers[res.HostId] = res
}

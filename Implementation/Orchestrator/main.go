package main

import (
	"TestOrchestrator/rtaudiohandle"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	ct := rtaudiohandle.NewController()
	r := gin.Default()
	r.Use(cors.Default())

	r.POST("/rta/register", ct.Register)
	r.POST("/rta/results", ct.Results)
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	api := r.Group("/api")

	api.GET("/hosts", ct.Index)
	api.POST("/rta/init", ct.InitTest)
	api.POST("/rta/cancel", ct.Cancel)
	api.POST("/rta/test", ct.RunTest)
	api.GET("/rta/results", ct.GetResults)
	if err := r.Run(":9999"); err != nil {
		panic(err)
	}
}

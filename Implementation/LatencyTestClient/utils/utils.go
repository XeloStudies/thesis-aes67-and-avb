package utils

import (
	"errors"
	"fmt"
	"net"
	"net/http"
	"time"
)

// SmallPoint is used to save the the change of the altitude
type SmallPoint struct {
	TS        int64       `json:"timestamp"`
	Sample    uint64      `json:"sample"`
	Value     interface{} `json:"value"`
	Latency   int         `json:"latency"`
	Underflow bool        `json:"underflow"`
	Overflow  bool        `json:"overflow"`
	Channel   uint64      `json:"channel"`
}

func NewSmallPoint() *SmallPoint {
	sp := SmallPoint{} //channel: 0
	sp.TS = time.Now().UnixNano()
	return &sp
}

type Pong struct {
	Orchestrator string
	LocalIP      string
}

func FindOrchestrator() (Pong, error) {
	nw, err := localNetworks()
	if err != nil {
		return Pong{}, err
	}

	for _, ipNet := range nw {
		ip, ipnet, err := net.ParseCIDR(ipNet.String())
		if err != nil {
			return Pong{}, nil
		}

		var ips []string
		for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
			ips = append(ips, ip.String())
		}
		// remove network address and broadcast address
		ips = ips[1 : len(ips)-1]
		p := checkIps(ips, ipNet.IP)
		if p != (Pong{}) {
			return p, nil
		}
	}
	return Pong{}, errors.New("No Orchestrator found...")
}

// checkIps, based on https://gist.github.com/kotakanbe/d3059af990252ba89a82
func checkIps(ips []string, currIP net.IP) Pong {
	concurrentMax := 1000
	pingChan := make(chan string, concurrentMax)
	pongChan := make(chan Pong, len(ips))
	oChan := make(chan Pong, 1)
	quit := make(chan int)
	for i := 0; i < concurrentMax; i++ {
		go ping(pingChan, pongChan, quit, currIP)
	}
	go getOrchestrator(len(ips), pongChan, quit, oChan)

	for _, ip := range ips {
		pingChan <- ip
	}
	o := <-oChan
	//fmt.Println(o)
	return o
}

// ping,based on https://gist.github.com/kotakanbe/d3059af990252ba89a82
func ping(pingChan <-chan string, pongChan chan<- Pong, quit chan int, ip net.IP) {
	for {
		select {
		case i := <-pingChan:
			myClient := &http.Client{
				Timeout: time.Duration(1) * time.Second,
			}
			r, err := myClient.Get("http://" + i + ":9999/ping")
			if err == nil {
				if r.StatusCode == http.StatusOK {
					pongChan <- Pong{Orchestrator: i, LocalIP: ip.String()}
				} else {
					pongChan <- Pong{Orchestrator: "", LocalIP: ""}
				}
			} else {
				pongChan <- Pong{Orchestrator: "", LocalIP: ""}
			}
		case <-quit:
			return
		}
	}
}

// getOrchestrator, based on pong from https://gist.github.com/kotakanbe/d3059af990252ba89a82
func getOrchestrator(length int, pongChan <-chan Pong, quit chan int, oChan chan Pong) {
	for i := 0; i < length; i++ {
		p := <-pongChan
		if len(p.Orchestrator) > 0 {
			oChan <- p
			quit <- 0
		}
	}
	fmt.Println("Nothing found")
	oChan <- Pong{Orchestrator: "", LocalIP: ""}
	quit <- 0
}

// localNetworks, Based on https://gist.github.com/miguelmota/8544989558d8723b42068aec5bc72ebf
func localNetworks() ([]*net.IPNet, error) {
	// Holle Alle Interfaces
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	var networks []*net.IPNet
	// Für jedes interface hole die Addressen
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			return nil, err
		}

		// Für jede Addresse
		for _, addr := range addrs {

			//fmt.Println(addr)
			switch v := addr.(type) {
			case *net.IPNet:
				if isPrivateIP(v.IP) {
					networks = append(networks, v)
				}
			}

		}
	}
	return networks, nil
}

// isPrivateIP, Based on https://gist.github.com/miguelmota/8544989558d8723b42068aec5bc72ebf
func isPrivateIP(ip net.IP) bool {
	var privateIPBlocks []*net.IPNet
	for _, cidr := range []string{
		// don't check loopback ips
		//"127.0.0.0/8", // IPv4 loopback
		//"::1/128",        // IPv6 loopback
		//"fe80::/10",      // IPv6 link-local
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
	} {
		_, block, _ := net.ParseCIDR(cidr)
		privateIPBlocks = append(privateIPBlocks, block)
	}

	for _, block := range privateIPBlocks {
		if block.Contains(ip) {
			return true
		}
	}

	return false
}

// inc increases the IP, based on https://gist.github.com/kotakanbe/d3059af990252ba89a82
func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

// LocalIP Based on https://gist.github.com/miguelmota/8544989558d8723b42068aec5bc72ebf
// LocalIP get the host machine local IP address
func LocalIP() (net.IP, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			return nil, err
		}

		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}

			if isPrivateIP(ip) {
				return ip, nil
			}
		}
	}

	return nil, errors.New("no IP")
}

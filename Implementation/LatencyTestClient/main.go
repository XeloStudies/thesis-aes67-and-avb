package main

import (
	"LatencyTests/api"
	"LatencyTests/stream"
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	fmt.Println("Starting client...")

	var rt stream.RtAudioWrap

	if err := rt.Init(); err != nil {
		fmt.Println("error:", err)
	}

	api, err := api.NewController(&rt).InitAPI()
	if err != nil {
		panic(err.Error())
	}

	if jsH, err := rt.MarshalHostinfo(); err != nil {
		fmt.Println("error:", err)
	} else {
		api.Register(jsH)
	}
	fmt.Println("Init finished, setup Server")
	r := gin.Default()
	r.POST("/init", api.InitStreamContext)
	r.GET("/stream/:runId", api.Stream)
	go func() {
		for _ = range sig {
			fmt.Println("\nInterrupted...")
			if rt.IsRunning() {
				fmt.Println("Stop the stream.")
				rt.Stop()
			} else {
				fmt.Println("Shutting down.")
				os.Exit(1)
			}
		}
	}()

	if err := r.Run(":9090"); err != nil {
		panic(err)
	}

}

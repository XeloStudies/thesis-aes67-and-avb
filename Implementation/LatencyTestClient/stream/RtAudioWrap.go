package stream

import (
	"LatencyTests/rtaudio-5.2.0/contrib/go/rtaudio"
	"LatencyTests/utils"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"runtime"
	"sync/atomic"
	"time"
)

// RtAudioWrap wraps the rtaudio.RtAudio struct for more accessibility
type RtAudioWrap struct {
	rtaudio.RtAudio
	Result      *Result
	Id          uint32
	ctx         *Context
	Runs        atomic.Uint64
	NumChannels uint
	IP          string
}

type Context struct {
	samples, sTotal atomic.Uint64
	raised          bool
	pointsCh        chan *utils.SmallPoint
}

func (rt *RtAudioWrap) ResetContext() {
	rt.ctx.sTotal.Store(0)
	rt.ctx.samples.Store(0)
	rt.ctx.raised = false
	rt.ctx.pointsCh = make(chan *utils.SmallPoint, 500)
}

// HostInfo describes the current host
type HostInfo struct {
	Name            string        `json:"name"`
	Id              uint32        `json:"id"`
	OperatingSystem string        `json:"os"`
	IpAddress       string        `json:"ip"`
	Devices         []*DeviceInfo `json:"devices"`
}

// DeviceInfo maps the rtaudio.DeviceInfo Struct to be json compatible.
type DeviceInfo struct {
	ID                  uint   `json:"id"`
	Name                string `json:"name"`
	Probed              bool   `json:"probed"`
	NumOutputChannels   int    `json:"numOutputChannels"`
	NumInputChannels    int    `json:"numInputChannels"`
	NumDuplexChannels   int    `json:"numDuplexChannels"`
	IsDefaultOutput     bool   `json:"isDefaultOutput"`
	IsDefaultInput      bool   `json:"isDefaultInput"`
	PreferredSampleRate uint   `json:"preferredSampleRate"`
	SampleRates         []int  `json:"sampleRates"`
}

func mapDeviceInfo(id int, info rtaudio.DeviceInfo) *DeviceInfo {
	return &DeviceInfo{
		ID:                  uint(id),
		Name:                info.Name,
		Probed:              info.Probed,
		NumOutputChannels:   info.NumOutputChannels,
		NumInputChannels:    info.NumInputChannels,
		NumDuplexChannels:   info.NumDuplexChannels,
		IsDefaultOutput:     info.IsDefaultOutput,
		IsDefaultInput:      info.IsDefaultInput,
		PreferredSampleRate: info.PreferredSampleRate,
		SampleRates:         info.SampleRates,
	}

}

func (rt *RtAudioWrap) Init() error {
	var err error
	var api rtaudio.API
	api = rtaudio.APILinuxALSA
	if runtime.GOOS == "windows" {
		api = rtaudio.APIWindowsWASAPI
	}

	rt.RtAudio, err = rtaudio.Create(api)

	fmt.Println(rt.RtAudio.CurrentAPI())
	if err != nil {
		fmt.Println("Could not create rt audio instance: ", err)
		return err
	}

	ds, err := rt.RtAudio.Devices()
	if err != nil {
		fmt.Println("Could not get devices: ", err)
		return err
	}
	fmt.Println(ds)
	for _, d := range ds {
		fmt.Println(d)
	}
	rt.ctx = &Context{
		samples:  atomic.Uint64{},
		sTotal:   atomic.Uint64{},
		raised:   false,
		pointsCh: nil,
	}

	rt.Runs.Store(0)
	return nil
}

func (rt *RtAudioWrap) InitPointsChan() {
	rt.ctx.pointsCh = make(chan *utils.SmallPoint, 500)
}

func (rt *RtAudioWrap) Devices() []*DeviceInfo {
	rtd, err := rt.RtAudio.Devices()
	if err != nil {
		panic(err)
	}
	var devices []*DeviceInfo
	for i, device := range rtd {
		devices = append(devices, mapDeviceInfo(i, device))
	}

	return devices
}

func (rt *RtAudioWrap) HostInfo() (HostInfo, error) {
	var host HostInfo
	var err error
	host.Id = rt.Id
	host.Devices = rt.Devices()
	if len(host.Devices) == 0 {
		host.Devices = make([]*DeviceInfo, 5)
		for i := 0; i < len(host.Devices); i++ {
			host.Devices[i] = &DeviceInfo{
				ID:                  uint(i),
				Name:                "Test",
				Probed:              false,
				NumOutputChannels:   0,
				NumInputChannels:    0,
				NumDuplexChannels:   0,
				IsDefaultOutput:     false,
				IsDefaultInput:      false,
				PreferredSampleRate: 0,
				SampleRates:         nil,
			}
		}
	}
	host.OperatingSystem = runtime.GOOS + " - " + runtime.GOARCH
	if host.Name, err = os.Hostname(); err != nil {
		return HostInfo{}, err
	}

	host.IpAddress = rt.IP

	return host, nil
}

func (rt *RtAudioWrap) MarshalHostinfo() ([]byte, error) {
	h, err := rt.HostInfo()
	if err != nil {
		return nil, err
	}
	jsd, err := json.Marshal(h)
	if err != nil {
		return nil, err
	}
	return jsd, nil
}

func (rt *RtAudioWrap) Stream(param *StreamParams) {
	fmt.Println("Streaming for ", param.Samples, " Samples")
	rt.NumChannels = param.NumChannels
	var f2 func(out rtaudio.Buffer, in rtaudio.Buffer, dur time.Duration, status rtaudio.StreamStatus) int
	switch param.Function {
	case "Pulse":
		fmt.Println("Streaming Pulse Function")
		f2 = newFuncGen(rt, param.Samples).Pulse
		break
	case "Steps":
		fmt.Println("Streaming Steps Function")
		f2 = newFuncGen(rt, param.Samples).Steps
		break
	default:
		errors.New("function: " + param.Function + "not found")
	}

	rt.ShowWarnings(true)

	if err := rt.Open(param.Convert(), nil, rtaudio.FormatInt8, param.SampleRate, param.FramesPerPacket, f2, nil); err != nil {
		fmt.Println(err)
	}
}

func (rt *RtAudioWrap) Receive(param *StreamParams) {
	fmt.Println("Receiving for ", param.Samples, " Samples")
	rt.NumChannels = param.NumChannels
	f2 := func(out rtaudio.Buffer, in rtaudio.Buffer, dur time.Duration, status rtaudio.StreamStatus) int {
		buf := in.Int8()
		sp := utils.NewSmallPoint()
		if status > 0 {
			fmt.Println("Overflow...")
			sp.Overflow = true
		}

		for i := 0; i < in.Len(); i++ {
			if buf[i] > 0 {
				rt.ctx.samples.Add(1)
				if buf[i] == 2 && !rt.ctx.raised {
					sp.Value = buf[i]
					sp.Sample = rt.ctx.samples.Load()
					if l, err := rt.Latency(); err == nil {
						sp.Latency = l
					} else {
						fmt.Println(err)
					}
					rt.ctx.pointsCh <- sp
					rt.ctx.raised = true
				}
				if rt.ctx.raised && buf[i] == 1 {
					sp.Value = buf[i]
					sp.Sample = rt.ctx.samples.Load()
					if l, err := rt.Latency(); err == nil {
						sp.Latency = l
					} else {
						fmt.Println(err)
					}
					rt.ctx.pointsCh <- sp
					rt.ctx.raised = false
				}
			}
			rt.ctx.sTotal.Add(1)
			if rt.ctx.samples.Load() >= param.Samples {
				if buf[i] == 3 {
					if rt.IsRunning() {
						rt.Result.TimeStopped = time.Now().UnixNano()
						fmt.Println("Finished with ", rt.ctx.sTotal.Load(), " total samples.")
						close(rt.ctx.pointsCh)
						rt.Result.SamplesTotal = rt.ctx.samples.Load()
						rt.Result.Duration = dur.Nanoseconds()
						rt.Stop()
					}

				}
			}
			if rt.ctx.sTotal.Load() > 2*param.Samples {
				fmt.Println("Killing receive stream, double expected Samples.")

				if rt.IsRunning() {
					rt.Result.TimeStopped = time.Now().UnixNano()
					fmt.Println("Finished with ", rt.ctx.sTotal.Load(), " total samples.")
					close(rt.ctx.pointsCh)
					rt.Result.SamplesTotal = rt.ctx.samples.Load()
					rt.Result.Duration = dur.Nanoseconds()
					rt.Stop()
				}
				return 0
			}
		}
		return 0
	}
	rt.ShowWarnings(true)

	if err := rt.Open(nil, param.Convert(), rtaudio.FormatInt8, param.SampleRate, param.FramesPerPacket, f2, nil); err != nil {
		fmt.Println(err)
	}
}

func (rt *RtAudioWrap) WritePoints(cb func()) {
	fmt.Println("Started Writing")
	for p := range rt.ctx.pointsCh {
		id := p.Channel
		rt.Result.Points[id] = append(rt.Result.Points[id], p)
	}
	fmt.Println("stopped Writing")
	fmt.Println("Points saved:", len(rt.Result.Points[0]))
	time.Sleep(time.Duration(1) * time.Second)
	cb()
}

func (rt *RtAudioWrap) CloseStream() {
	fmt.Println(rt.Result.Run, rt.Runs.Load()-1, rt.IsOpen())
	if rt.Result.Run != rt.Runs.Load()-1 {
		fmt.Println("Not all runs finished.")
		return
	}
	if rt.IsOpen() {
		fmt.Println("All runs finished, closing stream.")
		rt.Close()
	}
}

// StreamParams is the jsonized version of the rtaudio.StreamParams object.
type StreamParams struct {
	DeviceID        uint   `json:"deviceId"`
	NumChannels     uint   `json:"numChannels"`
	FirstChannel    uint   `json:"firstChannel"`
	Sender          bool   `json:"sender"`
	Receiver        bool   `json:"receiver"`
	FramesPerPacket uint   `json:"framesPerPacket"`
	SampleRate      uint   `json:"sampleRate"`
	Function        string `json:"function"`
	Samples         uint64 `json:"samples"`
	Runs            int    `json:"runs"`
}

// Convert translates the jsonized StreamParams Object to a rtaudio.StreamParams object
func (p StreamParams) Convert() *rtaudio.StreamParams {
	return &rtaudio.StreamParams{
		DeviceID:     p.DeviceID,
		NumChannels:  p.NumChannels,
		FirstChannel: p.FirstChannel,
	}
}

type FuncGenerator interface {
	Pulse(out rtaudio.Buffer, in rtaudio.Buffer, dur time.Duration, status rtaudio.StreamStatus) int
	Steps(out rtaudio.Buffer, in rtaudio.Buffer, dur time.Duration, status rtaudio.StreamStatus) int
}

type funcGenerator struct {
	rt         *RtAudioWrap
	val        int8
	maxSamples uint64
}

func newFuncGen(rt *RtAudioWrap, ms uint64) *funcGenerator {
	fg := &funcGenerator{rt: rt, val: 1, maxSamples: ms}
	return fg
}

func (g *funcGenerator) Pulse(out rtaudio.Buffer, in rtaudio.Buffer, dur time.Duration, status rtaudio.StreamStatus) int {
	buf := out.Int8()
	sp := utils.NewSmallPoint()
	if status > 0 {
		fmt.Println("Underflow")
		sp.Underflow = true
	}
	for i := 0; i < out.Len(); i++ {
		if g.rt.ctx.samples.Load() <= g.maxSamples {
			buf[i] = g.val
		} else {
			buf[i] = 3
		}

		g.rt.ctx.samples.Add(1)

		if g.rt.ctx.samples.Load() >= g.maxSamples {
			if g.rt.ctx.samples.Load() == g.maxSamples {
				g.rt.Result.TimeStopped = time.Now().UnixNano()
				fmt.Println("Finished with ", g.maxSamples, " samples.")
				// Write Last Point
				g.rt.Result.Duration = dur.Nanoseconds()
				close(g.rt.ctx.pointsCh)
				g.rt.Result.SamplesTotal = g.rt.ctx.samples.Load()

			}
			if g.rt.ctx.samples.Load() == g.maxSamples+500 {
				if g.rt.IsRunning() {
					fmt.Println("Stop the Stream")
					g.rt.Stop()
				}
				return 0
			}
		} else if g.rt.ctx.samples.Load()%10000 == 0 {
			if g.rt.ctx.raised {
				g.val = 1
				sp.Value = g.val
				sp.Sample = g.rt.ctx.samples.Load()
				if l, err := g.rt.Latency(); err == nil {
					sp.Latency = l
				} else {
					fmt.Println(err)
				}
				g.rt.ctx.pointsCh <- sp
				g.rt.ctx.raised = false
			} else {
				g.val = 2
				sp.Value = g.val
				sp.Sample = g.rt.ctx.samples.Load()
				if l, err := g.rt.Latency(); err == nil {
					sp.Latency = l
				} else {
					fmt.Println(err)
				}
				g.rt.ctx.pointsCh <- sp
				g.rt.ctx.raised = true
			}
		}
	}
	return 0
}

func (g *funcGenerator) Steps(out rtaudio.Buffer, in rtaudio.Buffer, dur time.Duration, status rtaudio.StreamStatus) int {
	//TODO implement me
	panic("implement me")
}

type Result struct {
	Id           uint32                `json:"id"`           // Id identifies the id of the host
	Run          uint64                `json:"runId"`        // Run is the run id of the current test
	Points       [][]*utils.SmallPoint `json:"points"`       //Points are the measured data points
	TimeStart    int64                 `json:"timeStart"`    // TimeStart, the run start
	TimeStopped  int64                 `json:"timeStopped"`  // TimeStopped, end of the run
	SamplesTotal uint64                `json:"samplesTotal"` // SamplesTotal, total amount of samples per test run
	Duration     int64                 `json:"duration"`     // Duration of Stream
	Latency      int                   `json:"latency"`      // Latency at the end of the Stream

}

func NewResult(id uint32, chs uint) *Result {
	points := make([][]*utils.SmallPoint, chs)
	return &Result{Id: id, Points: points, TimeStart: 0, TimeStopped: 0, SamplesTotal: 0}
}

func (r *Result) Marshal() ([]byte, error) {
	jsd, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return jsd, nil
}

# LatencyTestclient

LatencyTestclient is a client side distributed software based on [RtAudio](http://www.music.mcgill.ca/~gary/rtaudio/).

## Installation 

### Linux

To compile the software on Linux, RtAudio needs to be installed 
```bash
 wget http://www.music.mcgill.ca/~gary/rtaudio/release/rtaudio-5.2.0.tar.gz
 tar -xf rtaudio-5.2.0.tar.gz 
```

Set environment variable for Orchestrator:
```powershel
export LEADER_API='myHost'
```

### Windows

For the compilation in Windows RtAudio needs to be isntalled using [msys2](https://www.msys2.org/):
```bash
wget http://www.music.mcgill.ca/~gary/rtaudio/release/rtaudio-5.2.0.tar.gz
tar -xf rtaudio-5.2.0.tar.gz
```
Under windows the variable for Orchestrator is set by:
```powershel
$env:LEADER_API = 'myHost'
```
## Credits for Pipeline:
https://mike-ensor.medium.com/release-multi-target-rust-applications-with-gitlab-ci-90136fa10e4c 
https://gitlab.com/mike-ensor/gitlab-downloader 

package api

import (
	"LatencyTests/stream"
	"LatencyTests/utils"
	"bytes"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

type Controller struct {
	rt       *stream.RtAudioWrap
	api      string
	http     *http.Client
	finished chan bool
}

func NewController(rt *stream.RtAudioWrap) *Controller {
	myClient := &http.Client{
		Timeout: time.Duration(1) * time.Second,
	}
	return &Controller{rt: rt, api: "", http: myClient, finished: make(chan bool, 1)}
}

func (ct *Controller) InitAPI() (*Controller, error) {
	ip, err := utils.LocalIP()
	if err != nil {
		return nil, err
	}
	ct.rt.IP = ip.String()

	// First get env
	ct.api = os.Getenv("LEADER_API")
	fmt.Println(ct.api)

	if len(ct.api) > 0 {
		fmt.Println("API set to: ", ct.api)
		if ct.testAPI(ct.api) {
			return ct, nil
		}
	}

	// Second find orchestrator
	o, err := utils.FindOrchestrator()
	if err != nil {
		fmt.Println("Could not be found in any network...")
	} else {
		ct.api = "http://" + o.Orchestrator + ":9999"
		ct.rt.IP = o.LocalIP
		if ct.testAPI(ct.api) {
			return ct, nil
		}
	}

	// Last try localhost
	fmt.Println("No external API given. Try default: http://localhost:9999")
	ct.api = "http://localhost:9999"
	if ct.testAPI(ct.api) {
		return ct, nil
	}

	return nil, errors.New("no connection to any server")

}

func (ct *Controller) testAPI(api string) bool {
	_, err := ct.http.Get(api + "/ping")
	if err != nil {
		fmt.Println(err.Error())
		return false
	}
	return true
}

func (ct *Controller) Register(jsH []byte) {
	fmt.Println("Register to: ", ct.api)
	if resp, err := ct.http.Post(ct.api+"/rta/register", "application/json", bytes.NewBuffer(jsH)); err != nil {
		fmt.Println(err)
	} else {
		bodyBytes, err := io.ReadAll(resp.Body)
		closeBody := func() {
			if err := resp.Body.Close(); err != nil {
				fmt.Println("could not close body")
			}
		}
		defer closeBody()

		if err != nil {
			log.Fatal(err)
		}
		id, err := strconv.ParseUint(string(bodyBytes), 10, 32)
		if err != nil {
			fmt.Println("Cannot convert response ... ")
		}
		fmt.Println(id)
		ct.rt.Id = uint32(id)
	}
}

func (ct *Controller) InitStreamContext(c *gin.Context) {
	var param stream.StreamParams
	if err := c.BindJSON(&param); err != nil {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
	} else {
		if ct.rt.IsOpen() {
			c.JSON(http.StatusForbidden, gin.H{"error": "Stream already open"})
		}
		ct.rt.ResetContext()
		ct.rt.Runs.Store(uint64(param.Runs))
		if param.Sender == true {
			ct.rt.Stream(&param)
		} else if param.Receiver == true {
			ct.rt.Receive(&param)
		} else {
			c.Status(http.StatusUnprocessableEntity)
		}
	}
	c.Status(http.StatusOK)
}

func (ct *Controller) Stream(c *gin.Context) {
	ct.rt.Result = stream.NewResult(ct.rt.Id, ct.rt.NumChannels)
	ct.rt.ResetContext()
	var err error
	if ct.rt.Result.Run, err = strconv.ParseUint(c.Param("runId"), 10, 32); err != nil {
		fmt.Println("Could not convert runId", err)
		c.Status(http.StatusUnprocessableEntity)
	} else {
		fmt.Println(ct.rt.Result.Run, c.Param("runId"))
	}

	if ct.rt.IsOpen() {
		if !ct.rt.IsRunning() {
			fmt.Println("Stream is open and not running")
			if err := ct.rt.Start(); err == nil {
				ct.rt.Result.TimeStart = time.Now().UnixNano()
				fmt.Println("Starting write SmallPoint.")
				go ct.rt.WritePoints(ct.SendResult)

				c.Status(http.StatusOK)
			} else {
				fmt.Println("Could not start stream: ", err)
			}
		} else {
			fmt.Println("Stream still running")
		}
	} else {
		fmt.Println("Stream is closed ...")
		c.Status(http.StatusConflict)
	}

}

func (ct *Controller) SendResult() {
	fmt.Println("Sending Results")
	ps, err := ct.rt.Result.Marshal()
	if err != nil {
		panic(err)
	}

	if resp, err := ct.http.Post(ct.api+"/rta/results", "application/json", bytes.NewBuffer(ps)); err != nil {
		panic(err)
	} else if resp.StatusCode != http.StatusCreated {
		fmt.Println(resp.StatusCode)
	}
	ct.rt.CloseStream()
}

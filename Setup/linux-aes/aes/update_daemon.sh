#!/bin/bash 

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi
INF=""
while getopts i: opt
do
    case $opt in
        i) echo "setting inf"; INF=$OPTARG;;
    esac
done

AESdir=/opt/aes
if [[ ! -n $INF ]] ; then
    ei=$(ip -br l | awk '$1 !~ "lo|vir|wl" { print $1}')
    echo "Choose ethernet interface:" 
    i=0
    declare -a arr_eth
    for e in  ${ei[@]}; do
        echo "[${i}] ${e}"
        arr_eth+=($e)
        i=$(($i+1))
    done 
    read -r -p "" eth
    eth=$(($eth))
    INF=${arr_eth[$eth]}
else 
    echo "Use given interface:$INF"
fi

jq '.interface_name=$newVal' --arg newVal $INF $AESdir/daemon.conf > $AESdir/tmp.$$.conf && mv $AESdir/tmp.$$.conf $AESdir/daemon.conf

if [[ $(ps ax | grep aes67 | wc -l)  -gt 1 ]]; then
    /etc/init.d/aes67 restart
fi


#!/bin/bash

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi

read -r -p "Update/install dependencies? [y/n]" yn
if [[ "$yn" =~ ^(yes|y)$ ]]; then
    ./install_dependencies.sh
fi

currdir=$(pwd)

echo "Check for existing aes daemon"

# Need to clean
N2C=false

if [ -d /opt/aes ]; then
    while true; do
        echo "Already installed..."
        read -p "Clean up files? [y/N] " yn
        case "$yn" in
        [yY][eE][sS]|[yY]) 
            N2C=true
            break
            ;;
        [nN])
            N2C=false
            break
            ;;
        *) 
            echo "Please enter y or n"
            ;;
        esac
    done

    while true; do
        read -p "Backup daemon.conf [y/N] " yn
        case "$yn" in
        [yY][eE][sS]|[yY]) 
            mkdir tmp
            cp /opt/aes/daemon.conf $currdir/tmp/daemon.conf
            break
            ;;
        *)
            break
            ;;
        esac
    done
    echo "Removing daemon from registry"
    update-rc.d -f aes67 remove

    if [ $N2C = true ]; then
        ./cleanup.sh
    fi 
else
 echo "Fresh installing"
fi


mkdir -p /opt/aes

if [ -d /opt/aes ]; then
 echo "Created \"/opt/aes\""
else
 echo "Failed to create \"aes\" dir"
 exit 1
fi

if [[ -d $currdir/tmp && -f $currdir/tmp/daemon.conf ]]; then
    echo "Copy Backung daemon.conf"
    cp -t /opt/aes/ $currdir/tmp/daemon.conf
else 
    cp -t /opt/aes/ $currdir/daemon.conf
fi

read -r -p "Setup Network? This will configure the daemon... [y/n]" yn
if [[ "$yn" =~ ^(yes|y)$ ]]; then
    ./network.sh
fi

read -r -p "New installment of daemon?[y/n]: " di 
if [[ "$di" =~ ^(yes|y)$ ]]; then
    # Get and build AES67daemon
    ./getdaemon.sh
    [[ $? -gt 0 ]] && (echo "failed to install daemon"; exit 1)
    cp -t /opt/aes/ \
        $currdir/aes67-linux-daemon/daemon/aes67-daemon \
        $currdir/aes67-linux-daemon/daemon/scripts/ptp_status.sh \
        $currdir/aes67-linux-daemon/3rdparty/ravenna-alsa-lkm/driver/MergingRavennaALSA.ko

    cp -R $currdir/aes67-linux-daemon/webui /opt/aes/webui
fi 
echo "{}" > /opt/aes/status.json

[ -f /opt/aes/daemon.conf ] && echo "Copied files" || (echo "Files not copied ..."; exit 2 )

echo "New copy of init.d script..."
cp $currdir/aes67 /etc/init.d/aes67

cd /etc/init.d/
[[ -f aes67 && -x aes67 ]] && echo "File exists and is executable" || (echo "Change file to executable"; chmod 755 /etc/init.d/aes67 )

read -r -p "Should Test be executed?[y/n]: " testing
if [[ "$testing" =~ ^(yes|y)$ ]]; then
    echo "Test Daemon"
    /etc/init.d/aes67 start
    sleep 10
    curl -sf "$(hostname -I | xargs):8080" > /dev/null
    if [[ $?  -eq 0 ]]; then
        echo "Daemon successfully started";
    else
        echo "failed to start daemon";
        echo "See Logs: ";
        echo "/var/log/aes";
        echo "Last output:"
        tail /var/log/aes
        echo "\n"
    fi
    sleep 2
    /etc/init.d/aes67 stop
fi 
echo "Registring daemon"
update-rc.d aes67 defaults 

read -r -p "Install stream-actions to bin?[y/n]: " yn
if [[ "$yn" =~ ^(yes|y)$ ]]; then
    cp ../stream/media_listener.sh /usr/sbin/media_listener
    cp ../stream/start_stream.sh /usr/sbin/start_stream
fi
echo "Finished"
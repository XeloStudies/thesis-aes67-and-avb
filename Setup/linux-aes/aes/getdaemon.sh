#! /bin/bash

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi

currdir=$(pwd)
# Check for dependencies
needed=("git" "jq" "psmisc" "build-essential" "clang" "cmake" "libboost-all-dev" "valgrind" "linux-sound-base" "alsa-base" "alsa-utils" "libasound2-dev" "linuxptp" "libavahi-client-dev")
toinstall=""
for pkg in  ${needed[@]}; do
    [[ $(dpkg -s $pkg | grep Status | wc -l ) -ge 1 ]] || toinstall="$toinstall $pkg"
done
count=${#toinstall}
echo "$count dependencies to install"
if [[ $count -gt 0 ]]; then
    apt update && apt install $toinstall
fi

[ -d aes67-linux-daemon ] || git clone https://github.com/bondagit/aes67-linux-daemon.git

cd aes67-linux-daemon

./build.sh

echo "\n Build Complete "
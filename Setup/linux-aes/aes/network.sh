#!/bin/bash

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi 

read -r -p "Have all network cards been installed?[y/n]: " yn
if [[ "$yn" =~ ^(no|n)$ ]]; then
    echo "Please install first all needed networks cards"
    exit 1 
fi

# If disable file exists remove
# [[ -f /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg ]] && rm /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg

# echo "Disable automatic netplan config" 
# echo "To enable automatic config see /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg"
# echo "network: {config: disabled}" > /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg

ei=$(ip -br l | awk '$1 !~ "lo|vir|wl" { print $1}')
echo "Choose ethernet interface:" 
i=0
declare -a arr_eth
for e in  ${ei[@]}; do
    echo "[${i}] ${e}"
    arr_eth+=($e)
    i=$(($i+1))
done 

read -r -p "" eth
eth=$(($eth))

eth=${arr_eth[$eth]}

netplan set ethernets.$eth.dhcp4=false

if [[ $(( $(hostname | cut -d '-' -f 2) )) -gt 0 ]]; then
    ip=$((10+$(hostname | cut -d '-' -f 2) ))   
    netplan set ethernets.$eth.addresses="- 192.168.20.$ip/24"
    netplan set ethernets.$eth.gateway4=192.168.10.254
else
    read -r "Enter ip address: xxx.xxx.xxx.xxx/yy" ip
    netplan set ethernets.$eth.addresses="- 192.168.20.$ip/24"
    read -r "Enter gateway: " gw 
    netplan set ethernets.$eth.gateway4=$gw
fi

echo "Update Daemon..."
./update_daemon.sh -i $eth

echo "Update Nginx..."
#! /bin/bash
if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi
if [[ $(ps ax | grep aes67 | wc -l)  -gt 1 ]]; then
    echo "Shutting down daemon"
    /etc/init.d/aes67 stop
    update-rc.d -f aes67 remove
fi

echo "Clean up files"

[ -d /opt/aes ] && rm -R /opt/aes
[ -f /etc/init.d/aes67 ] && rm /etc/init.d/aes67

[[ -d $(pwd)/tmp && -f $(pwd)/cleanup.sh ]] && rm -R $(pwd)/tmp

if [[ -d $pwd/aes67-linux-daemon ]]; then
    read -rp "Remove current download?[y/n]: " yn
    if [[ "$di" =~ ^(yes|y)$ ]]; then
        rm -R aes67-linux-daemon
    fi
fi

[ -f /usr/sbin/media_listener ] && rm /usr/sbin/media_listener
[ -f /usr/sbin/start_stream ] && rm /usr/sbin/start_stream
echo "Cleanup finished."

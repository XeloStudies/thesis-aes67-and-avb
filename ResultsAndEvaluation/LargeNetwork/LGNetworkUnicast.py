#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import pandas as pd
import seaborn as sns
import scipy.stats as stats
sns.set_theme(style="ticks", palette="pastel")
files=['results_big_new/2to4Long.json','results_big_new/2to3Long.json']
longdf =pd.DataFrame()
name={}
name['3662568322']="ipmedia-1"
name['3645790703']="ipmedia-2"
name['3629013084']="ipmedia-3"
name['3612235465']="ipmedia-4"

for file in files: 
    print("Using File:" + file.split('/')[1].split('.')[0])
    f=open(file)
    data=json.load(f)
    data=data[0] 
    sdf=pd.DataFrame(data['sender']['points'][0])
    keys=list(data['receivers'].keys())
    key=''
    if len(keys) == 1: 
        key=keys[0]
    rdf=pd.DataFrame(data['receivers'][key]['points'][0])
    difdf=pd.DataFrame()
    difdf['dt_timestamp']=rdf['timestamp']-sdf['timestamp']
    difdf['dt_timestamp_ms']=difdf['dt_timestamp']/1000/1000
    difdf['dt_value']=rdf['value']-sdf['value']
    difdf['dt_sample']=rdf['sample']-sdf['sample']
    difdf['latency_s']=sdf['latency']
    difdf['latency_r']=rdf['latency']
    difdf['value']=sdf['value']
    difdf['sample']=sdf['sample']
    difdf['receiver'] = name[key]
    difdf['key']=int(key)
    difdf['dt_timestamp_ms']=difdf['dt_timestamp']/1000/1000
    difdf['index'] = difdf.index
    difdf['testcase'] = file.split('/')[1].split('.')[0]
    longdf=pd.concat([longdf, difdf])

ip3df=longdf[longdf['receiver'] == "ipmedia-3"]
ip4df=longdf[longdf['receiver'] == "ipmedia-4"]
    
longdf.head()



# In[2]:


print(ip3df['dt_timestamp_ms'].median())
ip3df.describe()


# In[3]:


print(ip4df['dt_timestamp_ms'].median())
ip4df.describe()


# In[4]:


ax=sns.boxplot(y=longdf['dt_timestamp_ms'],x=longdf['testcase'])
ax.set(xlabel='Test Case', ylabel='Latency [ms]')


# In[5]:


ax=sns.violinplot(x=longdf['value'], y=longdf['dt_timestamp_ms'])
ax.set(xlabel='Value', ylabel='Latency [ms]')


# In[6]:


ax=sns.violinplot(x=ip3df['value'], y=ip3df['dt_timestamp_ms'])
ax.set(xlabel='Value', ylabel='Latency [ms]')


# In[7]:


ax=sns.violinplot(x=ip4df['value'], y=ip4df['dt_timestamp_ms'])
ax.set(xlabel='Value', ylabel='Latency [ms]')


# In[8]:


from scipy.stats import ttest_ind
data1=longdf[longdf['testcase'] =='2to4Long']['dt_timestamp_ms']
data2=longdf[longdf['testcase'] =='2to3Long']['dt_timestamp_ms']
t_statistic, p_value = ttest_ind(data1, data2)
print("t-statistic:", t_statistic)
print("p-value:", p_value)


# In[ ]:





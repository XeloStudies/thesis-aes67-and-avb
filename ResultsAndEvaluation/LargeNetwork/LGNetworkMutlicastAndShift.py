#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import pandas as pd
import seaborn as sns
import scipy.stats as stats

sns.set_theme(style="ticks", palette="pastel")

files=['results_big_new/2to134Long.json']
longdf =pd.DataFrame()

dfs=[]
name={}
name['3662568322']="ipmedia-1"
name['3645790703']="ipmedia-2"
name['3629013084']="ipmedia-3"
name['3612235465']="ipmedia-4"

for file in files: 
    print("Using File:" + file.split('/')[1].split('.')[0])
    output=file.replace('.json','.csv')
    f=open(file)
    data=json.load(f)
    data=data[0] 
    sdf=pd.DataFrame(data['sender']['points'][0])
    keys=list(data['receivers'].keys())
    for key in keys: 
        #if key != "3662568322":
        #    continue 
        rdf=pd.DataFrame(data['receivers'][key]['points'][0])
        difdf=pd.DataFrame()
        difdf['dt_timestamp']=rdf['timestamp']-sdf['timestamp']
        difdf['dt_timestamp_ms']=difdf['dt_timestamp']/1000/1000
        difdf['dt_value']=rdf['value']-sdf['value']
        difdf['dt_sample']=rdf['sample']-sdf['sample']
        difdf['s_sample']=sdf['sample']
        difdf['r_sample']=rdf['sample']
        difdf['latency_s']=sdf['latency']
        difdf['latency_r']=rdf['latency']
        difdf['value']=sdf['value']
        difdf['sample']=sdf['sample']
        difdf['dt_timestamp_ms']=difdf['dt_timestamp']/1000/1000
        difdf['index'] = difdf.index
        difdf['testcase'] = file.split('/')[1].split('.')[0]
        difdf['receiver'] = name[key]
        difdf['key']=int(key)
        difdf['underflow']=rdf['underflow']
        difdf['overflow']=sdf['overflow']
        longdf=pd.concat([longdf, difdf])
        dfs.append(difdf)
        print(name[key])
ip1df=longdf[longdf['receiver'] == "ipmedia-1"]
ip3df=longdf[longdf['receiver'] == "ipmedia-3"]
ip4df=longdf[longdf['receiver'] == "ipmedia-4"]
longdf.head()


# In[2]:


print(ip1df['dt_timestamp_ms'].median())
ip1df.describe()


# In[3]:


print(ip3df['dt_timestamp_ms'].median())
ip3df.describe()


# In[4]:


print(ip4df['dt_timestamp_ms'].median())
ip4df.describe()


# In[5]:


ax=sns.lineplot(x='index', y='dt_timestamp_ms', data=dfs[2],linewidth=0.5)
ax.set(xlabel='Index', ylabel='Latency [ms]')


# In[6]:


ax=lplt2=sns.lineplot(x='index', y='dt_sample', data=dfs[2],linewidth=0.5)
ax.set(xlabel='Index', ylabel='Difference Samples')


# In[37]:


ax=lplt2=sns.lineplot(x='index', y='underflow', data=dfs[2],linewidth=0.5)
ax.set(xlabel='Index', ylabel='Underflow', ylim=(-1, 1))


# In[8]:


shorten2to34 = longdf[longdf['receiver'] != "ipmedia-1"]
ax=sns.boxplot(y=shorten2to34['dt_timestamp_ms'],x=shorten2to34['receiver'])
ax.set(xlabel='Receiver', ylabel='Latency [ms]')


# In[9]:


ax=sns.violinplot(x=shorten2to34['receiver'], y=shorten2to34['dt_timestamp_ms'])
ax.set(xlabel='Receiver', ylabel='Latency [ms]')


# In[10]:


from scipy.stats import ttest_ind
data1=shorten2to34[shorten2to34['receiver'] =='ipmedia-4']['dt_timestamp_ms']
data2=shorten2to34[shorten2to34['receiver'] =='ipmedia-3']['dt_timestamp_ms']

t_statistic, p_value = ttest_ind(data1, data2, equal_var=True, alternative='less')
print("t-statistic:", t_statistic)
print("{:e}".format(p_value))
print("p-value:", p_value)


# In[24]:


files=['results_big_new/2to134Long.json']
phasedfs =pd.DataFrame()

name={}
name['3662568322']="ipmedia-1"
name['3645790703']="ipmedia-2"
name['3629013084']="ipmedia-3"
name['3612235465']="ipmedia-4"

for file in files: 
    print("Using File:" + file.split('/')[1].split('.')[0])
    output=file.replace('.json','.csv')
    f=open(file)
    data=json.load(f)
    data=data[0] 
    sdf=pd.DataFrame(data['sender']['points'][0])
    keys=list(data['receivers'].keys())
    for key in keys: 
        if key == "3662568322":
            continue 
        rdf=pd.DataFrame(data['receivers'][key]['points'][0])
        rdf['receiver'] = name[key]
        rdf['key']=int(key)
        phasedfs=pd.concat([phasedfs, rdf])

gdf=phasedfs.groupby('receiver')
ip4df=gdf.get_group('ipmedia-4')
ip3df=gdf.get_group('ipmedia-3')
gdf.get_group('ipmedia-4')


# In[26]:


phaseDf=pd.DataFrame()
phaseDf['dt43']=ip4df['timestamp']-ip3df['timestamp']
phaseDf['dt43_ms']=phaseDf['dt43']/1000000


# In[27]:


print(phaseDf.median())
phaseDf.describe()


# In[39]:


ax=sns.displot(
    phaseDf, x="dt43_ms",
    binwidth=0.01, height=5, facet_kws=dict(margin_titles=True),
)
ax.set(xlabel='Shift [ms]')




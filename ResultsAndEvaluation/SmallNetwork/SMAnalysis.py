#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import pandas as pd
import seaborn as sns

file='results_small/l2l_long.json'
sns.set_theme(style="ticks", palette="pastel")

print("Using File:" + file.split('/')[1].split('.')[0])
f=open(file)
data=json.load(f)
data=data[0] 
sdf=pd.DataFrame(data['sender']['points'][0])
keys=list(data['receivers'].keys())
key=''
if len(keys) == 1: 
    key=keys[0]
    
rdf=pd.DataFrame(data['receivers'][key]['points'][0])
difdf=pd.DataFrame()
difdf['dt_timestamp']=rdf['timestamp']-sdf['timestamp']
difdf['dt_timestamp_ms']=difdf['dt_timestamp']/1000/1000
difdf['dt_value']=rdf['value']-sdf['value']
difdf['dt_sample']=rdf['sample']-sdf['sample']
difdf['value']=sdf['value']
difdf['sample']=sdf['sample']
difdf['dt_timestamp_ms']=difdf['dt_timestamp']/1000/1000
difdf['index'] = difdf.index
difdf['testcase'] = file.split('/')[1].split('.')[0]


difdf.head()


# In[2]:


print(difdf['dt_timestamp_ms'].median())
difdf.describe()


# In[3]:


ax=sns.boxplot(y=difdf['dt_timestamp_ms'])
ax.set(xlabel='ipmedia-4', ylabel='Latency [ms]')


# In[4]:


ax=sns.displot(difdf, x="dt_timestamp_ms")
ax.set(ylabel='Latency [ms]')


# In[5]:


ax=sns.violinplot(x=difdf['value'], y=difdf['dt_timestamp_ms'])
ax.set(xlabel='Value', ylabel='Latency [ms]')


# In[6]:


# Part Wide
file='results_small/l2l_wide.json'
#file='results_big_new/2to4wide.json'
print("Using File:" + file)
output=file.replace('.json','.csv')
f=open(file)
data=json.load(f)

receiverid=list(data[0]['receivers'].keys())[0]
sdf=pd.DataFrame()
rdf=pd.DataFrame()
for i in range(0,len(data)):
    sdfi=pd.DataFrame(data[i]['sender']['points'][0])
    sdfi['run']=i+1
    rdfi=pd.DataFrame(data[i]['receivers'][receiverid]['points'][0])
    rdfi['run']=i+1
    sdf=pd.concat([sdf,sdfi])
    rdf=pd.concat([rdf,rdfi])
    
difdf=pd.DataFrame()
difdf['run']=sdf['run']
difdf['dt_timestamp']=rdf['timestamp']-sdf['timestamp']
difdf['dt_timestamp_ms']=difdf['dt_timestamp']/1000/1000
difdf['dt_value']=rdf['value']-sdf['value']
difdf['value']=sdf['value']
difdf['sample']=sdf['sample']
difdf['index'] = difdf.index

difdf.describe()


# In[7]:


palette = sns.color_palette("rocket_r")
ax=sns.lineplot(x='index', y='dt_timestamp_ms', data=difdf, 
                  hue="run",  size_order=["T1", "T2"]
                  , palette=palette,
                 )
ax.set(xlabel='Index', ylabel='Latency [ms]')


# In[8]:


ltdf=difdf[difdf['dt_timestamp_ms'] > 0]


# In[9]:


ax=sns.lineplot(x='index', y='dt_timestamp_ms', data=ltdf, 
                  hue="run",  size_order=["T1", "T2"]
                  , palette=palette,
                 )
ax.set(xlabel='Index', ylabel='Latency [ms]')


# In[10]:


ax=sns.boxplot(y=ltdf['dt_timestamp_ms'],x=ltdf['run'])
ax.set(xlabel='Run', ylabel='Latency [ms]')


# In[11]:


ltdf.describe()


# In[12]:


ax=sns.violinplot(x=ltdf['value'], y=ltdf['dt_timestamp_ms'])
ax.set(xlabel='Value', ylabel='Latency [ms]')


# In[ ]:




